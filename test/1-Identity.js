const ethers = require('ethers')
const expect = require('chai').expect
const expectThrow = require('./helpers/expectThrow.js')
const truffleAssert = require('truffle-assertions')
const Identity = artifacts.require('./contracts/Identity.sol')
const Counter = artifacts.require('./contracts/Counter.sol')

// If ethers.js wasn't so rudimentary we could access these values
// through the contract interface, but it is and we can't.
const OPERATION_CALL = 0
const OPERATION_CREATE = 1
const OPERATION_INVALID = 333 // invented

const key = ethers.utils.formatBytes32String('test')
const valueStr = 'Hello World'
const value = ethers.utils.formatBytes32String(valueStr)
const idInterface = new ethers.utils.Interface(Identity.abi)
const counterInterface = new ethers.utils.Interface(Counter.abi)

contract('Identity', (accounts) => {
  const owner1 = accounts[1]
  const owner2 = accounts[1]
  const rando = accounts[3]
  let identity1, identity2

  it('Should Deploy the Identity Contract', async () => {
    identity1 = await Identity.new(owner1)
    expect(await identity1.owner()).to.be.equal(owner1)
  })

  it('Should test the Identity contract payable function', async () => {
    web3.eth.sendTransaction({ from: rando, to: identity1.address, value: web3.utils.toWei('1', 'ether') })
  })

  it('Should create Identity 2 from Identity 1', async () => {
    const encodedCall = idInterface.deployFunction.encode(Identity.bytecode, [owner1])
    const tx = await identity1.execute(OPERATION_CREATE, identity1.address, 0, encodedCall, { from: owner1 })

    truffleAssert.eventEmitted(tx, 'OwnershipTransferred', (ev) => {
      return ev.previousOwner === '0x0000000000000000000000000000000000000000' && ev.newOwner === identity1.address
    })
    truffleAssert.eventEmitted(tx, 'OwnershipTransferred', (ev) => {
      return ev.previousOwner === identity1.address && ev.newOwner === owner1
    })

    truffleAssert.eventEmitted(tx, 'ContractCreated', (ev) => {
      return ev.contractAddress !== null
    })

    const idAddress = tx.logs[2].args[0]
    identity2 = await Identity.at(idAddress)
    expect(await identity2.owner()).to.eq(owner1)
  })

  it('Should change back the owner to Identity 1', async () => {
    await identity2.transferOwnership(identity1.address, { from: owner1 })
    expect(await identity1.owner()).to.eq(owner1)
  })

  it('Should work as expected : setData and getData', (done) => {
    identity1.setData(key, value, { from: owner2 })
      .then(async (tx) => {
        expect(tx.logs[0].event).to.eq('DataChanged')
        return identity1.getData(key)
      })
      .then((resValue) => {
        expect(ethers.utils.parseBytes32String(resValue)).to.be.equal(valueStr)
        done()
      })
  })

  it('Should act as a proxy and change Owner again', async () => {
    const encodedCall = idInterface.functions.transferOwnership.encode([owner2])
    const tx = await identity1.execute(OPERATION_CALL, identity2.address, 0, encodedCall, { from: owner1 })
    truffleAssert.eventEmitted(tx, 'OwnershipTransferred', (ev) => {
      return ev.previousOwner === identity1.address && ev.newOwner === owner2
    })
    expect(await identity2.owner()).to.eq(owner2)
  })

  it('Should not waste gas on an invalid transaction type', async () => {
    await expectThrow(identity1.execute(OPERATION_INVALID, identity1.address, 0, value, { from: owner2 }))
  })

  it('Should be able to send MetaTX', async () => {
    // Create a new wallet and set it as owner for identity2
    let counter = await Counter.new(owner1)
    let wallet = ethers.Wallet.createRandom()
    let tx = await identity2.transferOwnership(wallet.address, { from: owner2 })
    expect(await identity2.owner()).to.eq(wallet.address)

    truffleAssert.eventEmitted(tx, 'OwnershipTransferred', (ev) => {
      return ev.contractAddress !== null
    })

    // Prepare the metatx
    const encodedCall = counterInterface.functions.incrementCounter.encode([])
    const hash = await identity2.getHash(counter.address, 0, encodedCall)

    // Sign the message.
    let flatSig = wallet.signingKey.signDigest(hash)
    let signature = ethers.utils.splitSignature(flatSig)
    expect(ethers.utils.recoverAddress(hash, signature)).to.eq(wallet.address)

    // Try the metatx
    expect(Number(await counter.getCount())).to.eq(0)
    tx = await identity2.metatx(counter.address, 0, encodedCall, signature.v, signature.r, signature.s)
    truffleAssert.eventEmitted(tx, 'MetaTxDone', (ev) => {
      return (ev.from === accounts[0] && ev.to === counter.address && ev.signed == wallet.address)
    })

    expect(Number(await counter.getCount())).to.eq(1)
  })
})
