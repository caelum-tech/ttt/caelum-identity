const ethers = require('ethers')
const expect = require('chai').expect
const expectThrow = require('./helpers/expectThrow.js')
const Identity = artifacts.require('./contracts/Identity.sol')
const IdFactory = artifacts.require('./contracts/IdFactory.sol')

const OPERATION_CALL = 0

contract('IdFactory', (accounts) => {
  let factory

  const owner = accounts[0]

  // Certificate Authority 1
  const ca1 = accounts[1]
  const ca1h = ethers.utils.formatBytes32String('ca1')

  // Certificate Authority 2
  const ca2 = accounts[2]
  const ca2h = ethers.utils.formatBytes32String('ca2')

  // Identity Owner 1
  const id1 = accounts[3]
  const id1h = ethers.utils.formatBytes32String('test1')

  it('Should Deploy the Factory Contract', async () => {
    factory = await IdFactory.new()
    expect(await factory.owner()).to.be.equal(owner)
  })

  it('Should Add a new Identity', async () => {
    // Add identity
    await factory.createIdentity(ca1h, ca1)
    expect(Number(await factory.numIdentifiers())).to.be.equal(1)

    // Check Identity contract.
    const newId = await factory.resolve(ca1h)
    const identifier = await Identity.at(newId)
    expect(await identifier.owner()).to.be.equal(ca1)
  })

  it('Adding another Identity should increase the number of identities', async () => {
    await factory.createIdentity(ca2h, ca2)
    expect(Number(await factory.numIdentifiers())).to.be.equal(2)
  })

  it('Should Not duplicate handlers', async () => {
    await expectThrow(factory.createIdentity(ca2h, ca2))
  })

  it('Should add authorities', async () => {
    // add autority 1. Max level 3.
    await factory.addAuthority(ca1h, 3)
    // Only owner can add authorities.
    await expectThrow(factory.addAuthority(ca2h, 9, { from: ca2 }))
    // add autority 2. Max level 9.
    await factory.addAuthority(ca2h, 9)
  })

  it('Should set the level for a user', async () => {
    // Set Level to a user.
    await factory.createIdentity(id1h, id1)
    const authority1 = await Identity.at(await factory.resolve(ca1h))

    const iface = new ethers.utils.Interface(IdFactory.abi)
    const call1 = iface.functions.setLevel.encode([id1h, '3'])
    await authority1.execute(OPERATION_CALL, factory.address, 0, call1, { from: ca1 })
    expect(Number(await factory.level(id1h))).to.be.equal(3)

    // Try to set a level higher than the actual permissions. And fail.
    const call2 = iface.functions.setLevel.encode([id1h, '6'])
    authority1.execute(OPERATION_CALL, factory.address, 0, call2, { from: ca1 })
    expect(Number(await factory.level(id1h))).to.be.equal(3)
    // await factory.setLevel(id1h, 3, { from: ca1 })
    // await expectThrow(factory.setLevel(id1h, 6, { from: ca1 }))
  })
})
