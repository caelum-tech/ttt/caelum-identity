pragma solidity ^0.5.4;
import "./Identity.sol";
import "./Ownable.sol";

contract IdFactory is Ownable{

    uint32 public numIdentifiers = 0;
    struct Identifier {
        address did;    // person delegated to
        uint version;   // index of the voted proposal
        uint level;     // Verification level
    }
    mapping(bytes32=>Identifier) identifiers;

    // Whitellist of Certificate Authorities.
    mapping(address=>uint) authorities;

    function createIdentity(bytes32 handler, address _owner) public {
        assert(identifiers[handler].version == 0);
        address newId = address(new Identity(_owner));
        identifiers[handler] = Identifier({
            did: newId,
            version: 1,
            level: 1
        });
        numIdentifiers++;
        // emit new Identity
    }

    function addAuthority(bytes32 handler, uint maxLevel) public onlyOwner {
        address did = identifiers[handler].did;
        authorities[did] = maxLevel;
    }

    function removeAuthority(bytes32 handler) public onlyOwner {
        address did = identifiers[handler].did;
        authorities[did] = 0;
    }

    function setLevel(bytes32 handler, uint level) public {
        require(authorities[msg.sender] >= level, "CA: caller is not an authorized CA");
        identifiers[handler].level = level;
    }

   /**
     * Returns the address associated with an ENS node.
     * @param handler The NS handler to query.
     * @return The associated address.
     */
    function resolve(bytes32 handler) public view returns (address) {
        return identifiers[handler].did;
    }

    /**
     * Returns the address associated with an ENS node.
     * @param handler The NS handler to query.
     * @return The associated level.
     */
    function level(bytes32 handler) public view returns (uint) {
        return identifiers[handler].level;
    }
}