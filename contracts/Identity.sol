pragma solidity ^0.5.4;
import "./ERC725.sol";
import "./Ownable.sol";

// Identity Contract.
contract Identity is ERC725, Ownable {

    uint256 constant OPERATION_CALL = 0;
    uint256 constant OPERATION_CREATE = 1;

    // Events.
    event MetaTxDone(address indexed from, address indexed to, address indexed signed);

    // Key/Value Store.
    mapping(bytes32 => bytes) store;

    // Nonce for Meta transactions.
    uint public nonce;

    // Set the ownership to the new Owner.
    constructor(address _owner) public {
        nonce = 0;
        transferOwnership(_owner);
    }

    // ----------------
    // Public functions
    function () external payable {}

    function getData(bytes32 _key)
        external
        view
        returns (bytes memory _value)
    {
        return store[_key];
    }

    function setData(bytes32 _key, bytes calldata _value)
        external
        onlyOwner
    {
        store[_key] = _value;
        emit DataChanged(_key, _value);
    }

    function execute(uint256 _operationType, address _to, uint256 _value, bytes memory _data)
        public onlyOwner
    {
        if (_operationType == OPERATION_CALL) {
            executeCall(_to, _value, _data);
        } else if (_operationType == OPERATION_CREATE) {
            address newContract = executeCreate(_data);
            emit ContractCreated(newContract);
        } else {
            // We don't want to spend user's gas if a parameter is wrong
            revert("invalid parameters");
        }
    }

    function getHash(address _to, uint256 _value, bytes memory _data)
        public view returns(bytes32)
    {
        return keccak256(abi.encodePacked(this.owner(), address(this), _to, _value, _data, nonce));
    }

    function metatx(
        address _to,
        uint256 _value,
        bytes calldata _data,
        uint8 v,
        bytes32 r,
        bytes32 s)
        external
    {
        // Check the tx is signed by the owner of the contract.
        bytes32 _hash = getHash(_to, _value, _data);

        // Increment the hash so this tx can't run again
        nonce++;
        address signed_by = ecrecover(_hash, v, r, s);

        // if (checkSigner(_hash, signature)) {
        if (signed_by == this.owner()) {
            executeCall(_to, _value, _data);
            emit MetaTxDone(msg.sender, _to, signed_by);
        }
    }

    // copied from GnosisSafe
    // https://github.com/gnosis/safe-contracts/blob/v0.0.2-alpha/contracts/base/Executor.sol
    function executeCall(address to, uint256 value, bytes memory data)
        internal
        returns (bool success)
    {
        // solium-disable-next-line security/no-inline-assembly
        assembly {
            success := call(gas, to, value, add(data, 0x20), mload(data), 0, 0)
        }
    }

    // copied from GnosisSafe
    // https://github.com/gnosis/safe-contracts/blob/v0.0.2-alpha/contracts/base/Executor.sol
    function executeCreate(bytes memory data)
        internal
        returns (address newContract)
    {
        // solium-disable-next-line security/no-inline-assembly
        assembly {
            newContract := create(0, add(data, 0x20), mload(data))
        }
    }
}
