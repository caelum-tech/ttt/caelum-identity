pragma solidity ^0.5.4;

// from https://github.com/ERC725Alliance/erc725/blob/6764caab5a840ad8e727ed92ca9066ae49cc1568/contracts/contracts/ERC725.sol
interface ERC725 {
    event DataChanged(bytes32 indexed key, bytes value);
    event ContractCreated(address indexed contractAddress);

    function getData(bytes32 _key) external view returns (bytes memory _value);
    function setData(bytes32 _key, bytes calldata _value) external;
    function execute(uint256 _operationType, address _to, uint256 _value, bytes calldata _data) external;
}
