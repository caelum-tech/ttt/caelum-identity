[![codecov](https://codecov.io/gl/caelum-tech/caelum-identity/branch/master/graph/badge.svg)](https://codecov.io/gl/caelum-tech/caelum-identity)

# caelum-identity
This library contains the contracts and libraries for the identity functionality of the Caelum blockchain.

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/caelum-identity/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/caelum-identity/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/caelum-identity/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/caelum-identity/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/caelum-identity/commits/master)|

# Prerequisites
* Ubuntu 18.04
* Node 10.15

# Install
```
npm i
```

# Testing
```
npm run lint
npm test
```

# Parity
Start and stop the parity client
```
npm run start
npm run stop
```

# Build
build the JSON for the Identity contract
```
npm run build
```
